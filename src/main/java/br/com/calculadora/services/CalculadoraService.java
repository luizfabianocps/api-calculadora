package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;
import sun.security.provider.certpath.OCSPResponse;

import java.sql.PreparedStatement;

@Service
public class CalculadoraService {
        private int resultado;
        //private RespostaDTO respostaDTO;

    public RespostaDTO somar(Calculadora calculadora){

        for (Integer numero : calculadora.getNumeros()){
            resultado = resultado+numero;
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO subtracao(Calculadora calculadora){
        for (Integer numero:calculadora.getNumeros()){
            resultado = resultado - numero;
        }
       return new RespostaDTO(resultado);
    }

    public RespostaDTO multiplicacao(Calculadora calculadora){
        for (Integer numero:calculadora.getNumeros()) {
            resultado *= numero;
        }
        return new RespostaDTO(resultado);
    }

    public RespostaDTO divisao(Calculadora calculadora){
        Float divisao;
        for (Integer numero:calculadora.getNumeros()){
            resultado = resultado / numero;
        }
         return new RespostaDTO(resultado);
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

//    public RespostaDTO getRespostaDTO() {
//        return respostaDTO;
//    }
//
//    public void setRespostaDTO(RespostaDTO respostaDTO) {
//        this.respostaDTO = respostaDTO;
//    }
}
