package br.com.calculadora.controllers;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST ,"É necessario, pelo menos 2 numeros");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            System.out.println("É necessário ao menos dois números para realizar a subtracao");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return calculadoraService.subtracao(calculadora);
    }

    @PostMapping("/multiplicacao")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            System.out.println("É necessário ao menos dois números para realizar a multiplicação");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return calculadoraService.multiplicacao(calculadora);
    }

    public RespostaDTO divisao(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            System.out.println("É necessário ao menos dois números para realizar a divisão");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return calculadoraService.divisao(calculadora);
    }


}
